from main import bubble_sort, insertion_sort, quicksort, shell_sort, data

def test_bubble_sort_decrease():
    assert bubble_sort([9, 8, 7, 6, 5, 4, 3, 2, 1, 0]) == [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
    assert bubble_sort([0, 1, 2, 3, 4, 5, 6, 7, 8, 9]) == [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
    assert bubble_sort([8, 7, 9, 3, 2, 5, 6, 1, 0, 4]) == [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]

def test_insertion_sort():
    assert insertion_sort([9, 8, 7, 6, 5, 4, 3, 2, 1, 0]) == [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
    assert insertion_sort([0, 1, 2, 3, 4, 5, 6, 7, 8, 9]) == [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
    assert insertion_sort([8, 7, 9, 3, 2, 5, 6, 1, 0, 4]) == [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]

def test_shell_sort():
    assert shell_sort([9, 8, 7, 6, 5, 4, 3, 2, 1, 0]) == [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
    assert shell_sort([0, 1, 2, 3, 4, 5, 6, 7, 8, 9]) == [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
    assert shell_sort([8, 7, 9, 3, 2, 5, 6, 1, 0, 4]) == [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]

def test_quicksort():
    assert quicksort([9, 8, 7, 6, 5, 4, 3, 2, 1, 0]) == [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
    assert quicksort([0, 1, 2, 3, 4, 5, 6, 7, 8, 9]) == [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
    assert quicksort([8, 7, 9, 3, 2, 5, 6, 1, 0, 4]) == [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]

def test_data_valid():
    for _ in range(len(data)):
        assert type(data[_]) == float or type(data[_]) == int
